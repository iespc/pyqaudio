import sys
from math import log, log2, pow, ceil
import numpy as np
np.seterr(divide = 'ignore') 
from PyQt6 import QtCore, QtGui, QtWidgets
import ui_main
import icons_rc
import pyqtgraph as pg
import pyaudio
import peakutils
from recorder import SoundCardDataSource

NAME = "PyQAudio"
VERSION = "0.5.3"

def getFFT(data,rate):
    """
    https://github.com/ricklupton/livefft
    Given some data and rate, returns FFTfreq and FFT (half).
    """
    data=data*np.hamming(len(data))    #hamming, bartlett, blackman, hanning, kaiser
    fft=np.fft.fft(data)
    fft=np.abs(fft)
    #fft=10*np.log10(fft)
    freq=np.fft.fftfreq(len(fft),1.0/rate)
    return freq[:int(len(freq)/2)],fft[:int(len(fft)/2)]


def rfftfreq(n, d=1.0):
    """
    https://github.com/ricklupton/livefft
    Return the Discrete Fourier Transform sample frequencies
    (for usage with rfft, irfft).

    The returned float array `f` contains the frequency bin centers in cycles
    per unit of the sample spacing (with zero at the start). For instance, if
    the sample spacing is in seconds, then the frequency unit is cycles/second.

    Given a window length `n` and a sample spacing `d`::

    f = [0, 1, ..., n/2-1, n/2] / (d*n) if n is even
    f = [0, 1, ..., (n-1)/2-1, (n-1)/2] / (d*n) if n is odd

    Unlike `fftfreq` (but like `scipy.fftpack.rfftfreq`)
    the Nyquist frequency component is considered to be positive.

    Parameters
    ----------
    n : int
    Window length.
    d : scalar, optional
    Sample spacing (inverse of the sampling rate). Defaults to 1.

    Returns
    -------
    f : ndarray
    Array of length ``n//2 + 1`` containing the sample frequencies.
    """
    if not isinstance(n, int):
        raise ValueError("n should be an integer")
    val = 1.0/(n*d)
    N = n//2 + 1
    results = np.arange(0, N, dtype=int)
    return results * val


def fft_slices(x):
    """
    https://github.com/ricklupton/livefft
    """
    Nslices, Npts = x.shape
    window = np.hanning(Npts)

    # Calculate FFT
    fx = np.fft.rfft(window[np.newaxis, :] * x, axis=1)

    # Convert to normalised PSD
    Pxx = abs(fx)**2 / (np.abs(window)**2).sum()

    # Scale for one-sided (excluding DC and Nyquist frequencies)
    Pxx[:, 1:-1] *= 2

    # And scale by frequency to get a result in (dB/Hz)
    # Pxx /= Fs
    return Pxx ** 0.5


def find_peaks(Pxx):
    """
    https://github.com/ricklupton/livefft
    """
    # filter parameters
    b, a = [0.01], [1, -0.99]
    Pxx_smooth = filtfilt(b, a, abs(Pxx))
    peakedness = abs(Pxx) / Pxx_smooth

    # find peaky regions which are separated by more than 10 samples
    peaky_regions = nonzero(peakedness > 1)[0]
    edge_indices = nonzero(diff(peaky_regions) > 10)[0]  # RH edges of peaks
    edges = [0] + [(peaky_regions[i] + 5) for i in edge_indices]
    if len(edges) < 2:
        edges += [len(Pxx) - 1]

    peaks = []
    for i in range(len(edges) - 1):
        j, k = edges[i], edges[i+1]
        peaks.append(j + np.argmax(peakedness[j:k]))
    return peaks


def fft_buffer(x):
    """
    https://github.com/ricklupton/livefft
    """
    window = np.hanning(x.shape[0])

    # Calculate FFT
    fx = np.fft.rfft(window * x)

    # Convert to normalised PSD
    Pxx = abs(fx)**2 / (np.abs(window)**2).sum()

    # Scale for one-sided (excluding DC and Nyquist frequencies)
    Pxx[1:-1] *= 2

    # And scale by frequency to get a result in (dB/Hz)
    # Pxx /= Fs
    return Pxx ** 0.5

class MainApp(QtWidgets.QMainWindow, ui_main.Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainApp, self).__init__(parent)
        pg.setConfigOption('background', 'w') #before loading widget
        self.setupUi(self)
        self.setWindowTitle(NAME)
        self.setWindowIcon(QtGui.QIcon(":/icons/audio-input-microphone.svg"))
        # Class variables
        self.started = False
        self.devices = {}
        self.maxFFT=0
        self.maxPCM=0
        self.restart = True
        self.paused = False
        self.Pxx = None
        self.downsample = False
        # Additional Gui Widgets
        self.toolBar = QtWidgets.QToolBar()
        self.toolBar.setContextMenuPolicy(QtCore.Qt.ContextMenuPolicy.PreventContextMenu)
        self.toolBar2 = QtWidgets.QToolBar()
        self.toolBar2.setContextMenuPolicy(QtCore.Qt.ContextMenuPolicy.PreventContextMenu)
        self.toolBar3 = QtWidgets.QToolBar()
        self.toolBar3.setContextMenuPolicy(QtCore.Qt.ContextMenuPolicy.PreventContextMenu)
        for tb in [self.toolBar, self.toolBar2, self.toolBar3] : 
            tb.setMovable(False)
            self.addToolBar(tb)
        self.grPCM.setTitle('Oscillogramme')
        self.grPCM.setMenuEnabled(False)
        self.grFFT.setMenuEnabled(False)
        self.plotPCM = self.grPCM.plot()
        self.grFFT.setTitle('FFT')
        self.plotFFT = self.grFFT.plot()
        self.plotFFT.setPen(pg.mkPen(color='r'))
        self.grPCM.plotItem.showGrid(True, True, 0.7)
        self.grFFT.plotItem.showGrid(True, True, 0.7)
        self.plotPCM.setPen(pg.mkPen(color='b'))
        self.startIcon = QtGui.QIcon(":/icons/media-playback-start.svg")
        self.pauseIcon = QtGui.QIcon(":/icons/media-playback-pause.svg")
        self.pbSource = QtWidgets.QPushButton(QtGui.QIcon(":/icons/audio-input-microphone.svg"), "")
        self.pbSource.setFlat(True)
        self.pbSource.setStyleSheet("QPushButton { border: none;}") #No border on buttons make them non-clickable
        self.acStartPause = self.buildAction(self.startIcon, "Start/Pause", shortcut = "SPACE", tooltip = "Démarrer/mettre en pause\n[Barre d'espace]", triggered = self.startPause)
        self.acPeak=self.buildAction(QtGui.QIcon(":/icons/view-object-histogram-linear.svg"), tooltip = "Chercher les pics", triggered = self.findPeaks)
        self.acCoord=self.buildAction(QtGui.QIcon(":/icons/tool-measure.svg"), tooltip = "Mesurer les fréquences", checkable = True, toggled = self.anFFT.dispCoord)
        self.acExit=self.buildAction(QtGui.QIcon(":/icons/application-exit.svg"), tooltip = "Quitter l'application", triggered = self.quit)
        self.acAbout=self.buildAction(QtGui.QIcon(":/icons/help-about.svg"), tooltip = "A propos", triggered = self.about)        
        self.acHelp=self.buildAction(QtGui.QIcon(":/icons/system-help.svg"), tooltip = "Aide", triggered = self.helpMe)
        self.acFullScreen = self.buildAction(QtGui.QIcon(":/icons/view-fullscreen.svg"), checkable = True, shortcut = "F11", tooltip = "Plein écran", triggered = self.fullscreen)
        self.acNotes = self.buildAction(QtGui.QIcon(":/icons/folder-sound.svg"), checkable = True, tooltip = "Afficher les notes de musique")
        tbw = [self.acStartPause]
        self.populateToolbar(self.toolBar, tbw)
        # self.toolBar.addSeparator()
        tb2w = [self.acCoord, self.acPeak, self.acNotes]
        self.populateToolbar(self.toolBar2, tb2w)
        self.toolBar2.setEnabled(False)
        tb3w = ["-", self.acHelp, self.acAbout, self.acFullScreen, self.acExit]
        self.populateToolbar(self.toolBar3, tb3w)
        #Signals/Slots
        self.tabWidget.currentChanged.connect(self.acqAnalyse)
        self.acNotes.triggered.connect(self.toggleNotes)
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        QtCore.QTimer.singleShot(10,self.main_init) #waits for this to finish until gui displayed

    
    def main_init(self):
        app.processEvents()
        self.initCapture()
        
    
    def initCapture(self, FS = 44100, ms=100):
        chunk_size=ms/1000*FS
        self.device = self.get_default_device()
        try :
            self.recorder = SoundCardDataSource(self.device, num_chunks=1,
                                sampling_rate=FS,
                                chunk_size=chunk_size)
            # print(self.recorder)
            self.interval_ms = ceil(1000 * (self.recorder.chunk_size / self.recorder.fs))
            # print("Updating graphs every %.1f ms" % self.interval_ms)
            self.freqValues = rfftfreq(len(self.recorder.timeValues),
                                    1./self.recorder.fs)   
        except :
            self.critalcalMessageBox()

    def critalcalMessageBox(self):
        dlg = QtWidgets.QMessageBox(self)
        dlg.setWindowTitle("Périphérique de capture")
        dlg.setText("Aucun périphérique d'enregistrement n'a été détecté.")
        dlg.setIcon(QtWidgets.QMessageBox.Icon.Critical)
        dlg.setStandardButtons(QtWidgets.QMessageBox.StandardButton.Yes|QtWidgets.QMessageBox.StandardButton.No)
        buttonY = dlg.button(QtWidgets.QMessageBox.StandardButton.Yes)
        buttonY.setText('Réessayer')
        buttonN = dlg.button(QtWidgets.QMessageBox.StandardButton.No)
        buttonN.setText('Quitter')
        ans = dlg.exec()
        if ans == QtWidgets.QMessageBox.StandardButton.Yes :
            self.initCapture()
        else :
            self.quit()

    def update(self):
        if self.paused:
            return
        data = self.recorder.get_buffer()
        weighting = np.exp(self.recorder.timeValues / self.recorder.timeValues[-1])
        self.Pxx = fft_buffer(weighting * data[:, 0])

        #if self.downsample:
            #downsample_args = dict(autoDownsample=False,
                                   #downsampleMethod='subsample',
                                   #downsample=10)
        #else:
            #downsample_args = dict(autoDownsample=True)
        downsample_args = dict(autoDownsample=True)
        #self.grPCM.setData(x=self.recorder.timeValues, y=data[:, 0], **downsample_args)
        self.plotPCM.setData(self.recorder.timeValues, data[:, 0])
        #self.spec.setData(x=self.freqValues,
                          #y=(20*np.log10(Pxx) if self.logScale else Pxx))
        #fft = getFFT(data, self.recorder.fs)
        #self.plotFFT.setData(fft)
        #self.plotFFT.setData(x=self.freqValues,
                        #y=(20*np.log10(Pxx)))
        #print(len(self.freqValues))
        self.plotFFT.setData(x=self.freqValues[:],
                        y=self.Pxx[:])
                
    def pitch(self, frequency, system='fr'):
        """https://stackoverflow.com/questions/64505024/turning-a-frequency-into-a-note-in-python"""
        if system == 'fr' :
            NOTES = ['Do', 'Do#', 'Ré', 'Ré#', 'Mi', 'Fa', 'Fa#', 'Sol', 'Sol#', 'La', 'La#', 'Si']
            KNOWN_NOTE_NAME, KNOWN_NOTE_OCTAVE, KNOWN_NOTE_FREQUENCY = ('La', 3, 440)
        else :
            NOTES = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'] 
            KNOWN_NOTE_NAME, KNOWN_NOTE_OCTAVE, KNOWN_NOTE_FREQUENCY = ('A', 4, 440) # A4 = 440 Hz
        OCTAVE_MULTIPLIER = 2  
        note_multiplier = OCTAVE_MULTIPLIER**(1/len(NOTES))
        frequency_relative_to_known_note = frequency / KNOWN_NOTE_FREQUENCY
        distance_from_known_note = log(frequency_relative_to_known_note, note_multiplier)
        distance_from_known_note = round(distance_from_known_note)
        known_note_index_in_octave = NOTES.index(KNOWN_NOTE_NAME)
        known_note_absolute_index = KNOWN_NOTE_OCTAVE * len(NOTES) + known_note_index_in_octave
        note_absolute_index = known_note_absolute_index + distance_from_known_note
        note_octave, note_index_in_octave = note_absolute_index // len(NOTES), note_absolute_index % len(NOTES)
        note_name = NOTES[note_index_in_octave]
        return note_name + str(note_octave)
    
    def fullscreen(self):
        if self.acFullScreen.isChecked() :
            self.showFullScreen()
        else : 
            self.showNormal()        

    def buildAction(self, icon, text="", shortcut = None, tooltip = None, checkable = False, checked = False, visible = True, triggered = None, toggled = None):
        if isinstance(icon, QtGui.QPixmap) :
            icon = QtGui.QIcon(icon)
        if isinstance(icon, QtGui.QIcon) :
            action = QtGui.QAction(icon, text, self)
        if shortcut :
            action.setShortcut(shortcut)
        if tooltip:
            action.setToolTip(tooltip)
        if checkable :
            action.setCheckable(True)
            if checked :
                action.setChecked(checked)
        if visible :
            action.setVisible(visible)
        if triggered :
            action.triggered.connect(triggered)
        if toggled : 
            action.toggled[bool].connect(toggled)
        return action
        
    def populateToolbar(self, toolbar, w_list):
        if isinstance(toolbar, QtWidgets.QToolBar) and isinstance(w_list, list):
            for w in w_list :
                if isinstance(w, QtGui.QAction):
                    toolbar.addAction(w)
                elif isinstance(w, QtWidgets.QWidget):
                    toolbar.addWidget(w)
                elif w == "|" :
                    toolbar.addSeparator()
                elif w == "-" :
                    spacer = QtWidgets.QWidget()
                    spacer.setSizePolicy(QtWidgets.QSizePolicy.Policy.Expanding, QtWidgets.QSizePolicy.Policy.Expanding)
                    toolbar.addWidget(spacer)
                    
    def about(self):
        from aPropos import aProposDlg
        text = "Acquisition audio et analyse spectrale par fft"
        icon = QtGui.QIcon(":/icons/microphone-sensitivity-high.svg")
        Dialog=aProposDlg(icon = icon, info = "<html> <b><font size=5> {}</font></b><br>Version : {} </html>".format(NAME, VERSION), tabs=[("À propos",text)], license="gpl-3.0")
        Dialog.exec()
        
    def helpMe(self):
        current_index = self.tabWidget.currentIndex()
        if current_index == 0 :            
            text = """
            <html>
            </style></head><body style=" font-family:'Noto Sans'; font-size:10pt; font-weight:400; font-style:normal;">
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:12pt; font-weight:600;">Acquisition</span></p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:11pt; font-weight:600; color:#2e6c80;">Boutons</span><span style=" font-size:11pt; font-weight:600;"> </span></p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"> <img src=":/icons/media-playback-start.svg" style="vertical-align: middle;" /> Démarrer/arrêter l'acquisition (Barre d'espace) </p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"> <img src=":/icons/pyqtgraph_auto.svg" style="vertical-align: middle;" /> Zoom automatique (si modifié)</p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:11pt; font-weight:600; color:#2e6c80;">Utilisation de la souris</span></p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"> <img src=":/icons/input-mouse-click-left.svg" style="vertical-align: middle;" /> Déplacer les courbes</p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"> <img src=":/icons/input-mouse-click-right.svg" style="vertical-align: middle;" /> Modifier les échelles</p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"> <img src=":/icons/input-mouse-click-middle.svg" style="vertical-align: middle;" /> Molette : Zoom avant/arrière</p>
            </body></html>"""

        else :
            text = """
            <html>
            </style></head><body style=" font-family:'Noto Sans'; font-size:10pt; font-weight:400; font-style:normal;">
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:12pt; font-weight:600;">Analyse</span></p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:11pt; font-weight:600; color:#2e6c80;">Boutons</span><span style=" font-size:11pt; font-weight:600;"> </span></p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"> <img src=":/icons/tool-measure.svg" style="vertical-align: middle;" /> Mesurer à l'aide d'un curseur</p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"> <img src=":/icons/view-object-histogram-linear.svg" style="vertical-align: middle;" /> Détecter les pics et afficher les fréquences</p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"> <img src=":/icons/folder-sound.svg" style="vertical-align: middle;" /> Associer des notes aux pics</p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:11pt; font-weight:600; color:#2e6c80;">Utilisation de la souris</span></p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"> <img src=":/icons/input-mouse-click-left.svg" style="vertical-align: middle;" /> Déplacer les courbes</p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"> <img src=":/icons/input-mouse-click-right.svg" style="vertical-align: middle;" /> Modifier les échelles</p>
            <p style=" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"> <img src=":/icons/input-mouse-click-middle.svg" style="vertical-align: middle;" /> Molette : Zoom avant/arrière</p>
            </body></html>"""
            
        dlg = QtWidgets.QMessageBox(self)
        dlg.setWindowTitle(f"Aide - {self.tabWidget.tabText(current_index)}")
        dlg.setText(text)
        dlg.exec()
        
    def closeEvent(self, ev):        
        self.quit()            
            
    def quit(self):
        if self.started :
            self.timer.stop()
        self.close()
                
    def acqAnalyse(self, tab):
        if tab == 0 :   #Acquisition tab 
            self.toolBar2.setEnabled(False)
            self.toolBar.setEnabled(True)
        if tab == 1 :
            if self.started :
                self.startPause()
            self.toolBar.setEnabled(False)
            self.toolBar2.setEnabled(True)
            self.analyse()

    def analyse(self):
        #self.maxFFT=np.max(np.abs(self.Pxx))
        if self.Pxx is not None:
            self.anFFT.setData(self.freqValues[:],self.Pxx[:])


    def startPause(self):
        if self.started :
            self.timer.stop()
            self.acStartPause.setIcon(self.startIcon)
            self.started = False
            self.restart = True
        else : 
            self.acStartPause.setIcon(self.pauseIcon)
            self.timer.start(self.interval_ms)
            self.started = True

    def get_default_device(self):
        #https://gist.github.com/mansam/9332445
        p = pyaudio.PyAudio()
        info = p.get_host_api_info_by_index(0)
        numdevices = info.get('deviceCount')
        default = None
        for i in range (0,numdevices):
            if p.get_device_info_by_host_api_device_index(0,i).get('maxInputChannels')>0:
                name = p.get_device_info_by_host_api_device_index(0,i).get('name')
                self.devices[len(self.devices)] = (i,name )
        p.terminate()
        for i in range(len(self.devices)):
            dev = self.devices[i][1]
            if dev == "default" :
                default = self.devices[i]
                # print("Périphérique de capture:", default)
        return default
    
        
    def findPeaks(self):
        x,y = self.freqValues[:],self.Pxx[:]
        indexes = peakutils.indexes(y, thres=0.02, min_dist=5)
        pics = x[indexes]
        levels = y[indexes]
        fund = levels.argmax()
        # Highpass Filter
        hpf_pics = [x for x in pics if x > 60]
        #print(hpf_pics)
        self.listWidget.clear()
        for i in hpf_pics :
            fq = round(i,1)
            fq_str = "{} Hz".format(str(fq))
            if self.acNotes.isChecked() :
                fq_str += " ({})".format(str(self.pitch(fq)))
            self.listWidget.addItem(fq_str)

    def toggleNotes(self):
        self.findPeaks()


if __name__=="__main__":
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle('fusion')
    form = MainApp()
    form.show()
    app.exec()
