import sys
import numpy
from PyQt6 import QtCore, QtGui, QtWidgets
import pyqtgraph as pg
pg.setConfigOption('background', 'w')


class anFFT(pg.GraphicsLayoutWidget):    
    
    def __init__(self, parent=None):
        super(anFFT, self).__init__(parent)        
        self.label = pg.LabelItem(justify = "right")
        self.addItem(self.label)
        
        self.p = self.addPlot(row = 1, col = 0)
        self.plot = self.p.plot()
        self.p.setMenuEnabled(False)
        self.plot.setPen(pg.mkPen(color='r'))
        # crosshairs
        self.vLine = pg.InfiniteLine(angle=90, movable=False, pen='b')
        self.hLine = pg.InfiniteLine(angle=0, movable=False, pen='b')
        self.p.addItem(self.vLine, ignoreBounds=True)
        self.p.addItem(self.hLine, ignoreBounds=True)
        self.vb = self.p.vb
        self.vLine.hide()
        self.hLine.hide()

    #def mouseMoved(self, mousePoint):
        #curvePoint = self.vb.mapSceneToView(mousePoint)
        #if self.p.sceneBoundingRect().contains(mousePoint):
            ##index = int((curvePoint.x() - self.x[0]) * len(self.x))
            ##print(len(self.x), len(self.y))
            #index = int(curvePoint.x()*len(self.x))
            ##print(index, len(self.y))
            #if index > 0 and index < len(self.y):
                #self.label.setText("<span style='font-size: 12pt'>time=%0.5f,   \
                #<span style='color: blue'>curve=%0.5f</span>" % (self.x[index], self.y[index]))
                #self.vLine.setPos(curvePoint.x())
                #self.hLine.setPos(curvePoint.y())
            #else :
                #self.label.setText('')
                
                
    def mouseMoved(self, mousePoint):
        curvePoint = self.vb.mapSceneToView(mousePoint)
        if self.p.sceneBoundingRect().contains(mousePoint):
            #index = int((curvePoint.x() - self.x[0]) * len(self.x))
            #print(len(self.x), len(self.y))
            #index = int(curvePoint.x()*len(self.x))
            #print(index, len(self.y))
            #if index > 0 and index < len(self.y):
            self.label.setText("<span style='font-size: 12pt'> <span style= 'color: blue'>f = %0.1f Hz,   \
            </span >i = %0.1f </span >" % (curvePoint.x(), curvePoint.y()))
            self.vLine.setPos(curvePoint.x())
            self.hLine.setPos(curvePoint.y())
            #### AVEC UN TEST :
            #if x >=0 and y>=0:
                #self.label.setText("<span style='font-size: 12pt'> <span style= 'color: blue'>f = %0.1f Hz,   \
                #</span >i = %0.1f </span >" % (curvePoint.x(), curvePoint.y()))
                #self.vLine.setPos(curvePoint.x())
                #self.hLine.setPos(curvePoint.y())
            #else :
                #self.label.setText('')
                
    def dispCoord(self, state):
        if state == True :
            self.vLine.show()
            self.hLine.show()
            self.p.scene().sigMouseMoved.connect(self.mouseMoved)
        else :
            self.vLine.hide()
            self.hLine.hide()
            self.p.scene().sigMouseMoved.disconnect()
            self.label.setText('')

    def setData(self, x, y, **kargs):
        self.x = x
        self.y = y
        self.plot.setData(x, y, **kargs)
        self.p.enableAutoRange("xy", False)
        
    def setRange(self, *args, **kargs):
        self.p.setRange(*args, **kargs)
        
    def enableAutoRange(self, *args, **kargs):
        elf.p.enableAutoRange(*args, **kargs)
  
    #def test(self):
        #self.dispCoord(True)
        #self.x = numpy.linspace(-5., 5., 10000)
        #def gaussian(A, B, x):
            #return A * numpy.exp(-(self.x/(2. * B))**2.)
        #self.y = gaussian(5., 0.2, self.x)
        #i = 0
        #while i < 500:
            #noise = numpy.random.normal(0, .2, len(self.y))
            #y_new = self.y + noise
            #self.plot.setData(self.x, y_new, pen = "r", clear = True)
            #self.p.enableAutoRange("xy", False)
            #pg.QtGui.QApplication.processEvents()
            #i += 1
        #self.dispCoord(False)

if __name__=="__main__":
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle('fusion')
    form = anFFT()
    form.show()
    form.test()
    app.exec_()
