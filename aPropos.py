# -*- coding: utf-8 -*-


from PyQt6 import QtCore, QtGui, QtWidgets

from aPropos_ui import Ui_Dialog
import aPropos_rc

class aProposDlg(QtWidgets.QDialog):
    
    def __init__(self, info=None, icon = None, license = None, tabs = None):
        super(QtWidgets.QDialog, self).__init__()        
        #UI
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        
        self.ui.pushButton.clicked.connect(self.show_about_qt)
        
        known_licenses = ("gpl-2.0","gpl-3.0","lgpl-3.0")
        
        
        if not info :
            info = u"<html> <b><font size=5>PyQt About Dialog</font></b> <br> Version 0.1"
        self.ui.info.setText(info)
            
        if icon :
            if type(icon) == QtGui.QIcon :
                icon = icon.pixmap(64, 64, QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.On)
            if type(icon) == QtGui.QPixmap :
                self.ui.icon.setPixmap(icon)
            
            
        
        if not tabs :
            self.ui.tabWidget.hide()
            self.adjustSize()
        else :
            #print(tabs)
            for tab in tabs:
                #print(tab)
                t = QtWidgets.QWidget()
                gridLayout = QtWidgets.QGridLayout(t)
                tw = QtWidgets.QTextBrowser()
                tw.setOpenExternalLinks(True)
                tw.setText(tab[1])
                gridLayout.addWidget(tw, 0, 0, 1, 1)
                self.ui.tabWidget.addTab(t, tab[0])                        
        if license :
            t = QtWidgets.QWidget()
            gridLayout = QtWidgets.QGridLayout(t)            
            if license in known_licenses :
                source =  QtCore.QUrl("qrc:/licenses/{0}.html".format(license))
                tw = QtWidgets.QTextBrowser(t)
                tw.setSource(source)
            else :
                tw = QtWidgets.QTextEdit(t)
                tw.setText(license)
            gridLayout.addWidget(tw, 0, 0, 1, 1)
            self.ui.tabWidget.addTab(t, "Licence") 


    def show_about_qt(self):
        QtWidgets.QMessageBox.aboutQt(self, "")


