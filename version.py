import sys
print("Python : %s.%s.%s" % sys.version_info[:3])
from PyQt6.QtCore import QT_VERSION_STR
print("PyQt : {}".format(QT_VERSION_STR))
import numpy, pyaudio, pyqtgraph, peakutils
############ AVEC STR ################
#for i in ['numpy', 'pyqtgraph', 'pyaudio', 'peakutils']:
    #print("{} : {}".format(i, sys.modules[i].__version__))
    ############ SANS STR ################
for i in [pyqtgraph, numpy, pyaudio, peakutils]:
    print("{} : {}".format(i.__name__, i.__version__))
